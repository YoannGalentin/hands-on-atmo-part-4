# Atmo Workspace

## What is Atmo?

**[Atmo](https://github.com/suborbital/atmo)** is a project from [Suborbital](https://suborbital.dev/). 

To say it short, Atmo is a toolchain allowing writing and deploying wasm microservices smoothly.
With Atmo, you can write microservices in Rust, Swift, and AssemblyScript without worrying about complicated things, and you only have to care about your source code:

- No fight with wasm and string data type
- No complex toolchain to install
- Easy deployment with Docker (and then it becomes trivial to deploy wasm microservices on Kubernetes)
- ...

## About this project

This project is a sandbox with all that you need to start playing with Atmo right now without installing anything. So, **to get the benefit of that, you have to open this project with [Gitpod](https://www.gitpod.io/)**.

👋 You can see this project as a **"freestanding workshop"** (you can do it on your own).

With this project, you will learn to:

- Create your first Atmo project
- Modify your first Runnable (see it as a microservice or a function)
- Build and deploy your first Runnable
- Add a new Runnable to the project

### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/atmo-workspace)

### 🖐 To force the rebuild of the image, click on this button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#imagebuild/https://gitlab.com/k33g_org/discovering-atmo/atmo-workspace)

> - That will rebuild the image, without committing anything new to the `gitpod.dockerfile`.
> - Use case: Gitpod images might install packages that from time to time need update. The only way to update these packages is manually make a change in `gitpod.dockerfile`, which will initiate a rebuild to the image (and get the latest updates).

## 1️⃣ [Step 01] Create your first project

To create a new Atmo project, you'll use the **[subo CLI](https://github.com/suborbital/subo)** *(already installed in the Gitpod workspace)*:

```bash
subo create project services-demo
```
> `services-demo` is the name of the project

The **subo CLI** will create a project structure like below:

```bash
└── services-demo
   ├── Directive.yaml
   ├── Dockerfile
   └── helloworld
      ├── Cargo.toml
      └── src
         └── lib.rs
```

### Content of the project

The build of the generated project will create a "Runnable Bundle"; by default, this project contains 

- One function, named `helloworld` (all the code is in `helloworld/src/lib.rs`); we'll call this function a **Runnable** (and it's written in **Rust**)
- A `Directive.yaml` file where is defined the route to the function
- And a Dockerfile to embed the Bundle in a container and ease its deployment.

### Source code of the Runnable

The source code of the Runnable is pretty simple, and it's composed of a main `run` function. When you make an HTTP request to call the `helloworld` service, the `run` function is triggered and returns a `Result` type:

> extract of `services-demo/helloworld/src/lib.rs`
```rust
struct HelloWorld{}

impl Runnable for HelloWorld {
   fn run(&self, input: Vec<u8>) -> Result<Vec<u8>, RunErr> {
      let in_string = String::from_utf8(input).unwrap();
    
      Ok(String::from(format!("hello {}", in_string)).as_bytes().to_vec())
   }
}
```

👋 Before going to the next step, modify the value of the returned value:

```rust
Ok(String::from(format!("👋 hello {} 😃\n", in_string)).as_bytes().to_vec())
```

### Content of `Directive.yaml`

> extract of `services-demo/Directive.yaml`
```yaml
handlers:
  - type: request
    resource: /hello
    method: POST
    steps:
      - fn: helloworld
```

This means that when you will do a request on `http://your-domain/hello`, you will call the `helloworld` function.

## 2️⃣ [Step 02] Build and serve the Bundle

### Build the Bundle

To build the Bundle, type the following commands and wait for some seconds:

```bash
cd services-demo
subo build .
```

The **subo CLI** builds a **wasm** file `helloworld.wasm`, and generates a new file: `runnables.wasm.zip` at the root of the project containing `helloworld.wasm` and `Directive.yaml`:

```bash
.
└── services-demo
   ├── Directive.yaml
   ├── Dockerfile
   ├── helloworld
   │  ├── Cargo.lock
   │  ├── Cargo.toml
   │  ├── helloworld.wasm ⬅️👋
   │  └── src
   │     └── lib.rs
   └── runnables.wasm.zip ⬅️👋
```

### Serve the Bundle

```bash
docker run \
   -v /workspace/atmo-workspace/services-demo:/home/atmo \
   -e ATMO_HTTP_PORT=8080 \
   -p 8080:8080 \
   suborbital/atmo:latest atmo
```

✋✋✋ **There is a simpler way to run a development server: `subo dev`** *(in the project directory)*
> see the documentation: https://atmo.suborbital.dev/getstarted/building-and-running#running-a-development-server

Open a new terminal and type the below command to call your new service:

```bash
curl http://localhost:8080/hello -d 'Jane Doe'
# you'll get: 👋 hello Jane Doe 😃
```

> 👋 you can stop the service with `ctrl + c`

## 3️⃣ [Step 03] Add a new "function"

It's straightforward to add a new function (aka Runnable) thanks to the **subo CLI**. For example, if you want to add a new function written in **Swift**, type the below commands:

```bash
cd services-demo
subo create runnable hey --lang swift
```

A new directory is created:

```bash
├── hey
│  ├── Package.swift
│  └── Sources
│     └── hey
│        └── main.swift
```

### Examine and change the source code 

You'll find the source code in `/hey/Sources/hey/main.swift`:

```swift
import Suborbital

class Hey: Suborbital.Runnable {
   func run(input: String) -> String {
      return "hello " + input
   }
}

Suborbital.Set(runnable: Hey())
```

The principle is the same as in Rust, but the code is simpler. Change the returned string of the function like this:

```swift
func run(input: String) -> String {
   return "😍 hey " + input + "\n"
}
```

### Update the `Directive.yaml`

Before building the Runnable Bundle, you need to add a route to be able to call the new `hey` service. Then edit the `Directive.yaml` file and add the below source code to the `handlers` section:

```yaml
- type: request
   resource: /hey
   method: POST
   steps:
   - fn: hey
```

### Build and serve

Like in the previous step:

```bash
cd services-demo
subo build .
```

When the Bundle is created, serve it again:

```bash
docker run \
   -v /workspace/atmo-workspace/services-demo:/home/atmo \
   -e ATMO_HTTP_PORT=8080 \
   -p 8080:8080 \
   suborbital/atmo:latest atmo
```

✋ **Or more simply: `subo dev`**

Open a new terminal and type the below command to call your new service:

```bash
curl http://localhost:8080/hey -d 'John Doe'
# you'll get: 😍 hey John Doe
```

And of course, you can still call the previous service:

```bash
curl http://localhost:8080/hello -d 'Jane Doe'
# you'll get: 👋 hello Jane Doe 😃
```

> **Remark**: this Gitpod workspace exposes the http port `8080`, if you want to call the services from outside, you can use the `gp url 8080` to get a reachable url:
> ```
> url=$(gp url 8080)
> curl ${url}/hey -d 'John Doe'
> ```
> *use the value of `gp url 8080`*

## 4️⃣ [Step 04] Embed the Bundle in a container, and serve it

Every **Atmo** project contains a `Dockerfile`:

```bash
.
└── services-demo
   ├── Directive.yaml
   ├── Dockerfile ⬅️👋
   ├── helloworld
   │  ├── Cargo.lock
   │  ├── Cargo.toml
   │  └── src
   │     └── lib.rs
   ├── hey
   │  ├── Package.resolved
   │  ├── Package.swift
   │  └── Sources
   │     └── hey
   │        └── main.swift
   └── runnables.wasm.zip
```

It's very convenient to deploy your wasm services everywhere (especially on Kubernetes, stay tuned for a next workshop).

Build the container image like this:

```bash
cd services-demo
docker build -t services-demo .
```

> **Remark**: if you get an error message like `error checking context: 'can't stat ' ...`, type `sudo chown -R $USER .` and try again.

Now, you can simply run your container and serve the services like this:

```bash
docker run \
   -e ATMO_HTTP_PORT=8080 \
   -p 8080:8080 \
   services-demo:latest
```

And call your service again:

```bash
curl http://localhost:8080/hey -d 'John Doe'
# you'll get: 😍 hey John Doe
```

## 🆕 Other languages

**Suborbital** added the support of **[Grain](https://grain-lang.org/)** and **[TinyGo](https://tinygo.org/)**.

### Grain

To create a new wasm service written in **Grain**, use the below command:

```bash
subo create runnable yo --lang grain
```

The **subo CLI** will create a project structure like below:

```bash
yo
├── index.gr
└── lib.gr
```

The code of the function is located in `lib.gr`, you can update it like this:

```ocaml
import Bytes from "bytes"
import String from "string"

export let run: Bytes -> Result<Bytes, (Int32, String)> = input => {
  let name = Bytes.toString(input)
  let message = String.concat("🙂 hello ", name)
  let response = String.concat(message, "\n")

  Ok(Bytes.fromString(response))
}
```

You need to add a route to be able to call the new `yo` service. Then edit the `Directive.yaml` file and add the below source code to the `handlers` section:

```yaml
- type: request
   resource: /yo
   method: POST
   steps:
   - fn: yo
```

Once the Runnable built and running, you can call the service like that:

```bash
curl http://localhost:8080/yo -d 'John Doe'
```

### TinyGo

To create a new wasm service written in **Go** thanks to **TinyGo**, use the below command:

```bash
subo create runnable hi --lang tinygo
```

The **subo CLI** will create a project structure like below:

```bash
hi
├── go.mod
└── main.go
```

The code of the function is located in `main.go`, you can update it like this:

```go
type Hi struct{}

func (h Hi) Run(input []byte) ([]byte, error) {
	return []byte("🖐 Hello, " + string(input) + "\n"), nil
}
```

You need to add a route to be able to call the new `hi` service. Then edit the `Directive.yaml` file and add the below source code to the `handlers` section:

```yaml
- type: request
   resource: /hi
   method: POST
   steps:
   - fn: hi
```

Once the Runnable built and running, you can call the service like that:

```bash
curl http://localhost:8080/hi -d 'Jane Doe'
```


That's all for today 🎉
